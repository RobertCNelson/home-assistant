# home-assistant

from: https://github.com/home-assistant/supervised-installer

- 1.5.1: (mar 16) os-agent: https://github.com/home-assistant/os-agent/tags
- 1.4.3: (jun 3) homeassistant-supervised: https://github.com/home-assistant/supervised-installer/tags

# Notes
homeassistant-supervised: 1.5.0, Addes bookworm and needs systemd-resolved
https://github.com/home-assistant/supervised-installer/commit/15f4ab16c2d358a9eb6e24650fdcfb46d812f22d

#
